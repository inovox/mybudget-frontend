import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {ApplicationCookies} from '../functions/ApplicationCookies'

export const ProtectedRoute = ({ component: Component, ...rest }) => {
    const {isAuthenticated} = ApplicationCookies()

    return (
        <Route {...rest} render={
            props => {
                if (isAuthenticated === 'true') {
                    return <Component {...rest} {...props} />
                } else {
                    return <Redirect to={
                        {
                            pathname: '/unauthorized',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                }
            }
        } />
    )
}
