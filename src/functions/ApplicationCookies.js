export const ApplicationCookies = () => {
    let token = sessionStorage.getItem('token')
    let isAuthenticated = sessionStorage.getItem('isAuthenticated')

    return {
        token,
        isAuthenticated
    }
}
