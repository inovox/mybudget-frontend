import React from 'react'
import { Button, Spinner, Alert, ListGroup } from 'react-bootstrap'
import { useDispatch, useSelector } from "react-redux";
import { setResponseAPI, setLoading, setVariantAlert } from '../actions/applicationActions';
import { ApiDelete } from '../functions/ApiDelete'
import { useCentrosCusto } from '../hooks/useCentrosCusto'
import { NavigationBar } from '../components/NavigationBar'

export const CentroCustoListar = () => {
    let centrosCusto = useCentrosCusto()
    const dispatch = useDispatch();
    const { responseAPI, variantAlert, loading } = useSelector(state => state.applicationReducer);

    // exclusão de uma origem
    const handleExclusao = (id) => {
        /* zera variável de exibição do feedback do envio ao usuário */
        dispatch(setResponseAPI(""));

        /* atualiza status de loading no botão de envio */
        dispatch(setLoading(true))

        /* cria objeto com dados que serão enviados */
        const dataToSubmit = {
            id
        }

        /* cadastra nova origem */
        ApiDelete(dataToSubmit, 'centroCusto')
            .then((retorno) => {
                if (retorno) {
                    dispatch(setLoading(retorno.loading));
                    dispatch(setResponseAPI(retorno.responseAPI))
                    dispatch(setVariantAlert(retorno.variantAlert));
                }
            })
            .catch((error) => {
                if (error) {
                    dispatch(setLoading(error.loading));
                    dispatch(setResponseAPI(error.responseAPI))
                    dispatch(setVariantAlert(error.variantAlert));
                }
            })
    }

    return (
        <div>
            <NavigationBar />
            <div>
                {(responseAPI !== undefined && responseAPI !== "" && responseAPI !== " ") ? <Alert variant={variantAlert}> {responseAPI} </Alert> : <span></span>}
                <br />
                <h3>Centros de Custo</h3>
                <ListGroup>
                    {centrosCusto.map(origem => (
                        <ListGroup.Item key={origem._id}>
                            <p>{origem.descricao}</p>
                            <Button variant="danger" disabled={loading} onClick={(e) => handleExclusao(origem._id, e)}>
                                {loading && (<Spinner animation="border" variant="warning" size="sm" />)}
                                Excluir
                            </Button>
                        </ListGroup.Item>
                    ))}
                </ListGroup>
            </div>
        </div>
    )
}