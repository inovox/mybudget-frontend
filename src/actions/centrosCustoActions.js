export const SET_DESCRICAO = "SET_DESCRICAO";

export const setDescricao = (descricao) => ({
  type: SET_DESCRICAO,
  payload: descricao
});

